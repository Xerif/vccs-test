<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function index()
    {
        return \App\Item::with('category')->get();
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'category_id' => 'required|integer',
            'title'       => 'required|max:255',
            'description' => 'required',
        ]);

        $item = \App\Item::create($data);
        $item->category;

        return $item;
    }
}
