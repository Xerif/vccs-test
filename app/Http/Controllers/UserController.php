<?php

namespace App\Http\Controllers;

use App\Http\Middleware\RedirectIfNotAdministrator;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', RedirectIfNotAdministrator::class]);
    }

    public function index()
    {
        return \App\User::all();
    }
}
