<?php

namespace App\Http\Controllers;

use Auth;

class MenuController extends Controller
{
    public function index()
    {
        return \App\Menu::when(Auth::user()->type != 'administrator', function ($q) {
            return $q->where('show', 'all');
        })->get();
    }
}
