require('./bootstrap');

window.Vue = require('vue');
window.toastr = require('toastr');

toastr.options = {
  "closeButton": true,
  "newestOnTop": true,
  "positionClass": "toast-top-right",
  "progressBar": true,
  "showMethod": "slideDown",
  "timeOut": 8000,
  "extendedTimeOut": 5000,
};

/* MIX_SENTRY_DSN_PUBLIC=http://127.0.0.1:8000 */
const server = process.env.MIX_SENTRY_DSN_PUBLIC;

const app = new Vue({
    el: '#app',
    data: {
        uri: '/',
        menus: [],
        items: [],
        users: [],
        categories: [],
        searchText: '',
        searchCategory: 0,

    },
    mounted () {
        axios
            .get(server + '/menus')
            .then(response => (this.menus = response.data))
            .catch(error => (console.log(error)));

        axios
            .get(server + '/items')
            .then(response => (this.items = response.data))
            .catch(error => (console.log(error)));

        axios
            .get(server + '/users')
            .then(response => (this.users = response.data))
            .catch(error => (console.log(error)));

        axios
            .get(server + '/categories')
            .then(response => (this.categories = response.data))
            .catch(error => (console.log(error)));
    },
    methods: {
        getContent(uri) {
            this.uri = uri;
        },
        setCategory(id) {
            if(id == this.searchCategory)
                this.searchCategory = 0;
            else
                this.searchCategory = id;
        },
        refreshItems(uri) {
            axios
                .get(server + uri)
                .then(response => (
                    this.items = response.data,
                    toastr.success('Datos actualizados.')
                ))
                .catch(error => (
                    console.log(error),
                    toastr.error('Error al actualizar los datos.')
                ));
        },
        refreshUsers(uri) {
            axios
                .get(server + uri)
                .then(response => (
                    this.users = response.data,
                    toastr.success('Datos actualizados.')
                ))
                .catch(error => (
                    console.log(error),
                    toastr.error('Error al actualizar los datos.')
                ));
        },
        itemStore(event) {
            let elements = event.target.elements;
            let data = {
                title: elements.title.value,
                category_id: elements.category.value,
                description: elements.description.value 
            };

            axios
                .post(server + '/items/store', data)
                .then(response => (
                    this.items.push(response.data),
                    toastr.success('Guardado correctamente', response.data.title)
                ))
                .catch(error => (
                    this.errors(Object.values(error.response.data.errors)),
                    console.log(error.response)
                ));
        },
        userStore(event) {
            let elements = event.target.elements;
            let data = {
                name: elements.name.value,
                email: elements.email.value,
                password: elements.password.value,
                password_confirmation: elements.password_confirmation.value
            };

            axios
                .post(server + '/register', data)
                .then(response => (
                    toastr.success('Usuario creado correctamente', response.data.name),
                    this.refreshUsers('/users')
                ))
                .catch(error => (
                    this.errors(Object.values(error.response.data.errors)),
                    console.log(error.response)
                ));
        },
        errors(errors) {
            errors.forEach(function(error) {
              toastr.error(error)
            });
        }
    },
    computed: {
        titleFilter: function(){
          return this.items.filter((item) => {
            return item.title.toLowerCase().match(this.searchText.toLowerCase()) && (this.searchCategory == 0 || item.category_id == this.searchCategory);
          })
        }
    }
});
