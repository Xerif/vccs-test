<div class="card" v-if="uri === '/users/create'" @submit.prevent="userStore">
    <div class="card-header">{{__('Create client')}}</div>
    <div class="card-body">
        <form action="#" method="POST">
            <div class="form-group">
                <label for="name">{{ __('Name') }}</label>
                <input id="name" type="text" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="email">{{ __('E-Mail Address') }}</label>
                <input id="email" type="email" class="form-control" name="email" required autocomplete="off">
            </div>
            <div class="form-group">
                <label for="password">{{ __('Password') }}</label>
                <input id="password" type="password" class="form-control" name="password" required autocomplete="off">
            </div>
            <div class="form-group">
                <label for="password-confirm">{{ __('Confirm Password') }}</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="off">
            </div>
            <input type="submit" value="{{__('Save')}}" class="form-control btn btn-success">
        </form>
    </div>
</div>
