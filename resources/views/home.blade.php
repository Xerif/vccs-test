<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div>
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown nav-link">
                            {{ Auth::user()->name }}
                        </li>
                        <li>
                            <a class="nav-link" href="{{ route('logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <main class="py-4" id="app" v-cloak>
            <div class="container-fluit ml-1 mr-3">
                <div class="row">
                    <div class="col-12 col-sm-3 mb-3">
                        <div class="list-group" style="min-width: 200px">
                            <a href="#" 
                              class="list-group-item list-group-item-action" 
                              v-for="menu in menus" 
                              :title="menu.description" 
                              v-bind:class="[uri === menu.uri ? 'active' : 'text-dark']"
                              @click.prevent="getContent(menu.uri)">
                                <i :class="menu.icon"></i> 
                                @{{menu.title}}
                            </a>
                        </div>            
                    </div>
                    <div class="col-12 col-sm-9">
                        @include('welcome')
                        @include('items')
                        @include('itemsCreate')

                        @if(Auth::user()->type == 'administrator')
                            @include('users')
                            @include('usersCreate')
                        @endif
                    </div>
                </div>
            </div>
        </main>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>