<div class="card"  v-if="uri === '/'">
    <div class="card-header">Dashboard</div>
    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        {{__('Welcome')}}
    </div>
</div>