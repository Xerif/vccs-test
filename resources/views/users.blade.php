<div class="card"  v-if="uri === '/users'">
    <div class="card-header">
        {{__('Clients')}}
        <span class="float-right text-success" style="cursor:pointer;" @click="refreshUsers(uri)">
            <i class="fas fa-sync"></i>
        </span>
    </div>
    <div class="card-body">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Name')}}</th>
                    <th>{{__('E-Mail Address')}}</th>
                    <th>{{__('Type')}}</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="user in users">
                    <td>@{{user.id}}</td>
                    <td>@{{user.name}}</td>
                    <td>@{{user.email}}</td>
                    <td>@{{user.type}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>