<div class="card"  v-if="uri === '/items'">
    <div class="card-header">
        {{__('Items')}}
        <span class="float-right text-success" style="cursor:pointer;" @click="refreshItems(uri)">
            <i class="fas fa-sync"></i>
        </span>
    </div>
    <div class="card-body">
        <div class="mb-3">
            <input v-model="searchText" v-on:change="titleFilter" type="text" class="form-control mb-3" placeholder="{{__('Search')}}">
        </div>
        <div class="mb-3">
            <a href="#" 
              class="btn ml-1" 
              v-for="category in categories"
              v-bind:class="[searchCategory === category.id ? 'btn-success' : 'btn-info']"
              @click.prevent="setCategory(category.id)">
                @{{category.name}}
            </a>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>{{__('Title')}}</th>
                    <th>{{__('Description')}}</th>
                    <th>{{__('Category')}}</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="item in titleFilter">
                    <td>@{{item.id}}</td>
                    <td>@{{item.title}}</td>
                    <td>@{{item.description}}</td>
                    <td>@{{item.category.name}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>