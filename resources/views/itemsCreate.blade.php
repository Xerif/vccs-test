<div class="card" v-if="uri === '/items/create'" @submit.prevent="itemStore">
    <div class="card-header">{{__('Create items')}}</div>
    <div class="card-body">
        <form action="#" method="POST">
            <div class="form-group">
                <label for="category">{{__('Category')}}</label>
                <select name="category" class="form-control">
                    <option selected>{{__('Select a category')}}</option>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="title">{{__('Title')}}</label>
                <input type="text" name="title" class="form-control">
            </div>
            <div class="form-group">
                <label for="description">{{__('Description')}}</label>
                <textarea name="description" class="form-control"></textarea>
            </div>
            <input type="submit" value="{{__('Save')}}" class="form-control btn btn-success">
        </form>
    </div>
</div>