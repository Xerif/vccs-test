# [Ver demo](http://ec2-35-181-48-238.eu-west-3.compute.amazonaws.com)

<br>

Instalar proyecto
--
**Requisitos del servidor**


 - PHP >= 7.2.0
 - BCMath PHP Extension
 - Ctype PHP Extension
 - JSON PHP Extension
 - Mbstring PHP Extension
 - OpenSSL PHP Extension
 - PDO PHP Extension
 - Tokenizer PHP Extension
 - XML PHP Extension




Para ejecutar el proyecto debe realizar los siguientes pasos:

1. Clonar, instalar las dependencias y generar una key:
 ```
 git clone https://gitlab.com/Xerif/vccs-test.git
 composer install
 php artisan key:generate
 ```
2. Configurar la base de datos en el archivo `.env` o configurar una base de datos con los datos por defecto:

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=database_test
DB_USERNAME=test
DB_PASSWORD=test
```
3. Ejecutar las migraciones para crear las tablas y seeder para los datos por defecto: 

```
php artisan migrate:fresh --seed
```

4. Ejecutar el servidor integrado, por defecto arranca en `127.0.0.1:8000`, el proyecto esta configurado para trabajar en el puerto `8000` si por algún motivo se desea ejecutar en otro puerto puede configurarlo en el archivo `.env` (deberá compilar nuevamente los asset ejecutando `npm run production`):

```
php artisan serve
```
5. Abrir el navegador y abrir http://127.0.0.1:8000 los usuarios por defecto son:

```
// Usuario administrador
Email: admin@admin.es
Contraseña: password

// Usuario cliente
Email: client@client.es
Contraseña: password
```

----

Desarrollar una pequeña aplicación web que permita hacer lo siguiente:

1. Tendrá un login con dos tipos de usuarios: Administrador y usuario base.

2. Cada usuario al hacer login le aparecerá un menú vertical, las opciones
del menú serán cargadas de una tabla de la base de datos. En la tabla se 
almacenará, al menos, icono, título y descripción.

3. Al hacer clic en cada opción, cargar un contenido diferente, sin necesidad 
de cambio en la url (llamadas ajax).

4. Uno de las opciones del menú será un formulario y otra opción será un
listado que muestre lo insertado por formulario.Los campos del formulario son:

 - Categoría (desplegable con varias opciones)
 - Título (campo tipo texto)
 - Descripción (campo tipo textarea)
 - La lista de elementos debe tener un filtro de búsqueda tanto por 
título como por categorías. 
 - El filtro por categorías deber ser tipo botón, que al hacer clic 
muestra/oculta los elementos de la categoría sobre la que se ha 
realizado el clic.
 - El filtro por título será campo tipo texto.

5. El usuario administrador, además, podrá dar de alta nuevos usuarios.
Resultado: Mostrar un menú vertical, cada opción del menú lleva un icono, un 
título y una descripción. Habrá una capa central donde se muestre un 
contenido según la opción elegida en el menú. Una de ellas mostrará un 
registro de elementos que deberán ser listados con un buscador.


Resultado: Mostrar un menú vertical, cada opción del menú lleva un icono, un
título y una descripción. Habrá una capa central donde se muestre un
contenido según la opción elegida en el menú. Una de ellas mostrará un
registro de elementos que deberán ser listados con un buscador.


