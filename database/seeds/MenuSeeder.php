<?php

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('menus')->insert(
            [
                [
                    'title'       => 'Home',
                    'description' => 'Description home',
                    'icon'        => 'fas fa-home',
                    'uri'         => '/',
                    'show'        => 'all',
                ],
                [
                    'title'       => 'Crear items',
                    'description' => 'Description formulario',
                    'icon'        => 'fas fa-plus-square',
                    'uri'         => '/items/create',
                    'show'        => 'all',
                ],
                [
                    'title'       => 'Listar items',
                    'description' => 'Description listado de items',
                    'icon'        => 'fas fa-inbox',
                    'uri'         => '/items',
                    'show'        => 'all',
                ],
                [
                    'title'       => 'Crear cliente',
                    'description' => 'Description usuarios',
                    'icon'        => 'fas fa-user-plus',
                    'uri'         => '/users/create',
                    'show'        => 'administrator',
                ],
                [
                    'title'       => 'Listar clientes',
                    'description' => 'Description listado usuarios',
                    'icon'        => 'fas fa-users',
                    'uri'         => '/users',
                    'show'        => 'administrator',
                ],
            ]
        );
    }
}
