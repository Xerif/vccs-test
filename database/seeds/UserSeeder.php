<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert(
            [
                [
                    'name'              => 'Vccs admin',
                    'email'             => 'admin@admin.es',
                    'email_verified_at' => now(),
                    'password'          => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                    'type'              => 'administrator',
                    'remember_token'    => Str::random(10),
                ],
                [
                        'name'              => 'Vccs client',
                        'email'             => 'client@client.es',
                        'email_verified_at' => now(),
                        'password'          => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                        'type'              => 'client',
                        'remember_token'    => Str::random(10),
                    ],
                ]
        );
    }
}
