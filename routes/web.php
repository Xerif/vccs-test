<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth'])
    ->group(function () {
        Route::get('/', 'HomeController@index');
        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('/menus', 'MenuController@index')->name('menus');
        Route::get('/items', 'ItemController@index')->name('items');
        Route::post('/items/store', 'ItemController@store')->name('items.store');
        Route::get('/users', 'UserController@index')->name('users');
        Route::get('/categories', 'CategoryController@index')->name('categories');
    }
);
